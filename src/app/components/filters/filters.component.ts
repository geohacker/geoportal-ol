import { Component, OnInit, SimpleChanges, Output, Input, EventEmitter, OnChanges} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LayersService } from '../../services/layers.service';

interface FeatureItem {
  type: string;
  geometry: {
    type: string;
    coordinates: number[];
  };
  properties: any;
}

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css'],
})
export class FiltersComponent implements OnInit, OnChanges  {

  @Output() layerSearch = new EventEmitter();
  @Input() propertiersLand: any;
  
  constructor(
    private formBuilder: FormBuilder,
    private layerService: LayersService
  ) {}

  checkoutForm = this.formBuilder.group({
    value_search: '',
  });

  formdata: FormGroup;
  validateForm: Validators;
  lands: FeatureItem[];
  result_geosearch: any;
  result_geosearch_b: boolean;

  ngOnInit(): void {
    this.validateForm = this.formBuilder.group({
      value_search: [null, [Validators.required]],
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(this.propertiersLand)
    // changes.prop contains the old and the new value...
  }

  viewLand(land){
    this.layerSearch.emit(land)
  }

  cleanFilter(){
    this.layerSearch.emit('');
    this.lands = [];
    this.result_geosearch_b = false;
  }

  /**
   * Fetch lands by filter and emit response layer
   */
  searchLand() {
    let word = this.checkoutForm.value.value_search
    if (word){
    this.layerService
      .getDataGeosearch(word)
      .subscribe((response: any) => {
        if (response["success"]){
          this.result_geosearch_b = true
          this.lands = response["data"].features
          this.result_geosearch = "Consulta exitosa. Se detecto una búsqueda de tipo: " + response["data-type"] + ". "
          
        } else {
          this.result_geosearch = "No se encontraron resultados. Se detecto una búsqueda de tipo: " + response["data-type"] + ". "
        }
      });
    } else {
      this.layerSearch.emit('');
      this.lands = [];
      this.result_geosearch = null;
      this.result_geosearch_b = false;
    }
  }
}
