import { Component, OnInit} from '@angular/core';
import { LayersService } from './services/layers.service';
import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import { Stroke, Style, Fill , Text } from 'ol/style';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import GeoJSON from 'ol/format/GeoJSON';
import * as interaction from 'ol/interaction';
import * as proj from 'ol/proj';

// Proj4
import prj4 from 'proj4';
import { register } from 'ol/proj/proj4';
prj4.defs(
  'EPSG:9377',
  '+proj=tmerc +lat_0=4.0 +lon_0=-73.0 +k=0.9992 +x_0=5000000 +y_0=2000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs'
);
register(prj4);
// import toEPSG4326 from 'ol/proj/epsg4326';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  //Layers Select of component Layers
  selectLayersACC: any;
  layerSearchACC: any;
  // Instance Map
  map: Map;
  // Output to component layers
  currentMessageResultLayer: String;
  messageFilter: String;
  isLoadingLayerACC: Boolean;
  ifCardLayerACC: Boolean;
  dataLand: String;
  // Output properties land select
  propertiersLand: any;
  // Sources for Draw Map
  source = new VectorSource({ wrapX: false });
  layer = new VectorLayer({ source: this.source, });
  sourceDraw = new VectorSource({ wrapX: false });
  layerDraw = new VectorLayer({ source: this.sourceDraw, });
  
  currentLayers = {};
  vectorPredio = new VectorSource({
    features: [],
  });

  predioLayer = new VectorLayer({
    source: this.vectorPredio,
    style: new Style({
      stroke: new Stroke({
        color: '#BAA35A',
        width: 1,
      }),
      text: new Text({
        font: '20px Calibri',
        placement: 'point',
        fill: new Fill({
          color: 'black',
        }),
      }),
      fill: new Fill({
        color: 'rgba(255, 255, 0, 0)',
      }),
    }),
  });


  vectorConst = new VectorSource({
    features: [],
  });

  constLayer = new VectorLayer({
    source: this.vectorConst,
    style: new Style({
      stroke: new Stroke({
        color: 'red',
        width: 1,
      }),
    
      text: new Text({
        font: '20px Calibri',
        placement: 'point',
        fill: new Fill({
          color: 'black',
        }),
      }),
      fill: new Fill({
        color: 'rgba(25, 255, 0,  0)',
      }),
    }),
  });
  vectorFiltro = new VectorSource({
    features: [],
  });
  vectorSearch = new VectorSource({
    features: [],
  });

  geomFilter: string = 'clear';

  constructor(private layersService: LayersService) {
  }


  // Display selected layers in component layers
  selectLayers(value: any) {
    this.selectLayersACC = value;
    this.updateLayer();
  }

  layerSearch(value: any){
   this.vectorSearch.clear()
   this.addLayerData(value, 'search', '', this.vectorSearch)
  }

  methodExport($event){ 
    var feature = this.sourceDraw.getFeatures()[0]
    var bbox = feature.getGeometry().getExtent()
    var point_ne = proj.transform([bbox[0], bbox[1]], 'EPSG:9377', 'EPSG:4326');
    var point_xy = proj.transform([bbox[2], bbox[3]], 'EPSG:9377', 'EPSG:4326');
    var bbox_points = point_ne.concat(point_xy);
    this.layersService
    .getExportFeaturesByGeometrySelect(
      bbox_points
    )
  }


  currentFilter(value: any){
    if (value == 'point' || value == 'polygon' || value == 'Circle' ){ 
      this.geomFilter = value;
      let draw = new interaction.Draw({
        source: this.sourceDraw,
        type: this.geomFilter,
      }); 
      this.map.addInteraction(draw);
      draw.on('drawstart', function (e) {
        // To do, delete de features
      });
    }
    if(value == 'desactive'){
      this.removeInteractions()
    } 
    if(value == 'clear'){
      this.sourceDraw.clear()
      this.vectorFiltro.clear()
      this.messageFilter = ''
    }
    if(value == 'consultar'){
      this.vectorFiltro.clear()
      var feature = this.sourceDraw.getFeatures()[0]
      var bbox = feature.getGeometry().getExtent()
      var point_ne = proj.transform([bbox[0], bbox[1]], 'EPSG:9377', 'EPSG:4326');
      var point_xy = proj.transform([bbox[2], bbox[3]], 'EPSG:9377', 'EPSG:4326');
      var bbox_points = point_ne.concat(point_xy);

      this.layersService
      .getCountFeaturesByGeometryCircle(
        bbox_points
      ).subscribe((response: any) => {
        if (response['success']) {
          this.layersService
          .getLayerByBoundingBox(
            bbox_points,
            'land',
            this.map.getSize(),
            this.map.getView().getZoom()
          )
          .subscribe((response: any) => {
            if (response['success']) {
              this.addLayerData(
                response['data'],
                'land',
                '',
                this.vectorFiltro
              );
              this.isLoadingLayerACC = false;
              this.ifCardLayerACC = false;
            }
          });
          this.messageFilter =  `Predios Filtrados: ${response["count-lands"]}`
        } else {
        this.messageFilter =  `Error: ${response["extra"]}`
        }
      });

    }
  }

  removeFeaturesSource(){
    this.sourceDraw.clear()
    this.vectorFiltro.clear()
    this.messageFilter = ''
  }

  removeInteractions(){
    // Remove all interations of interaction Draw
    this.map.getInteractions().forEach((interation_active) => {
      if (interation_active instanceof interaction.Draw){
        this.map.removeInteraction(interation_active);
      }
    })
  }

  updateLayer(){
      this.isLoadingLayerACC = false;
      this.ifCardLayerACC = true;
      this.currentMessageResultLayer = '';
      var bbox = this.map.getView().calculateExtent(this.map.getSize());
      var point_ne = proj.transform([bbox[0], bbox[1]], 'EPSG:9377', 'EPSG:4326');
      var point_xy = proj.transform([bbox[2], bbox[3]], 'EPSG:9377', 'EPSG:4326');
      var bbox_points = point_ne.concat(point_xy);
  
      if (this.selectLayersACC[0].isCheck || this.selectLayersACC[1].isCheck) {
        if (this.selectLayersACC[0].isCheck) {
          this.getLayerFix(
            bbox_points,
            this.selectLayersACC[0].service_basename,
            this.selectLayersACC[0].service_label,
            this.selectLayersACC[0].label,
            this.vectorPredio,
            this.predioLayer
          );
        }
        if (this.selectLayersACC[1].isCheck) {
          this.getLayerFix(
            bbox_points,
            this.selectLayersACC[1].service_basename,
            this.selectLayersACC[1].service_label,
            this.selectLayersACC[1].label,
            this.vectorConst,
            this.constLayer
          );
        }
       
      } else {
        this.vectorPredio.clear();
        this.vectorConst.clear();
        this.isLoadingLayerACC = false;
        this.ifCardLayerACC = false;
      }
  }

  public sendData(value){
    this.dataLand = value
  }

  updateView(){
    this.vectorPredio.clear();
    this.vectorConst.clear();
    if (this.selectLayersACC){
      this.updateLayer();
    }
    this.isLoadingLayerACC = false;
    
  }

  ngOnInit(): void {
    this.initilizeMap();
    // Allows to select lands
    let select = new interaction.Select();
    this.map.addInteraction(select);
    
    
    select.on('select', function (e) {
      var features = e.target.getFeatures();
      var properties = features["array_"][0]["values_"];
      properties["id"] = features["array_"][0]["id_"];

     
    });
  }



  initilizeMap() {
    this.map = new Map({
      view: new View({
        center: [4800000, 2100000],
        zoom: 10,
        projection: 'EPSG:9377',
      }),
      layers: [new TileLayer({ source: new OSM() }), this.layer, this.layerDraw],
    });

    this.map.on('moveend', function () {
      let a = document.getElementById('updateView');  
      a.click();
    });

  }

  private getLayerFix(points: string, service_layer, label, layer, vectorSource, vectorLayer) {
    vectorSource.clear()
    this.isLoadingLayerACC = false;
    this.ifCardLayerACC = true;
    this.layersService
      .getLayerByBoundingBox(
        points,
        service_layer,
        this.map.getSize(),
        this.map.getView().getZoom()
      )
      .subscribe((response: any) => {
        if (response['success']) {
          this.addLayerDataFix(
            response['data'],
            service_layer,
            label,
            vectorSource,
            vectorLayer
          );
          this.isLoadingLayerACC = false;
          this.ifCardLayerACC = false;
        } else {
          this.isLoadingLayerACC = false;
          this.currentMessageResultLayer =
            this.currentMessageResultLayer.concat(
              'Ocurrio un error (',
              response['message'],
              ') con la capa ',
              layer,
              ' / '
            );
        }
      });
  }

  private getLayer(points: string, service_layer, label, layer, vectorSource) {
    vectorSource.clear()
    this.isLoadingLayerACC = true;
    this.ifCardLayerACC = true;
    this.layersService
      .getLayerByBoundingBox(
        points,
        service_layer,
        this.map.getSize(),
        this.map.getView().getZoom()
      )
      .subscribe((response: any) => {
        if (response['success']) {
          this.addLayerData(
            response['data'],
            service_layer,
            label,
            vectorSource
          );
          this.isLoadingLayerACC = false;
          this.ifCardLayerACC = false;
        } else {
          this.isLoadingLayerACC = false;
          this.currentMessageResultLayer =
            this.currentMessageResultLayer.concat(
              'Ocurrio un error (',
              response['message'],
              ') con la capa ',
              layer,
              ' / '
            );
        }
      });
  }

  addLayerDataFix(geojson, layer, label, vectorSource, vectorLayer) {
    vectorSource.clear();
    vectorSource.addFeatures(
      new GeoJSON({
        defaultDataProjection: 'EPSG:4326',
        featureProjection: 'EPSG:9377',
      }).readFeatures(geojson)
    );

    if (this.map.getLayers().getLength() === 2) {
      this.map.getLayers().getArray().pop();
    }
    this.map.addLayer(vectorLayer);
    if (layer == 'search'){
      this.map.getView().fit(vectorSource.getExtent());
    }

  }
  

  addLayerData(geojson, layer, label, vectorSource) {
    const styles = {
      land: new Style({
        stroke: new Stroke({
          color: 'yellow',
          width: 1,
        }),
        text: new Text({
          font: '20px Calibri',
          placement: 'point',
          fill: new Fill({
            color: 'black',
          }),
        }),
        fill: new Fill({
          color: 'rgba(255, 255, 0, 0.1)',
        }),
      }),
      search: new Style({
        stroke: new Stroke({
          color: 'rgba(255, 05, 25)',
          width: 1,
        }),
        text: new Text({
          font: '20px Calibri',
          placement: 'point',
          fill: new Fill({
            color: 'black',
          }),
        }),
        fill: new Fill({
          color: 'rgba(100, 5, 50, 0.8)',
        }),
      }),
      building: new Style({
        stroke: new Stroke({
          color: 'rgba(25, 25, 25)',
          width: 1,
        }),
        text: new Text({
          font: '20px Calibri',
          placement: 'point',
          fill: new Fill({
            color: 'black',
          }),
        }),
        fill: new Fill({
          color: 'rgba(100, 25, 250, 0.8)',
        }),
      }),
    };

    const styleFunction = (feature) => {
      // styles.land.getText().setText(feature.get(label));
      return styles[layer];
    };

  
    vectorSource.clear();
    vectorSource.addFeatures(
      new GeoJSON({
        defaultDataProjection: 'EPSG:4326',
        featureProjection: 'EPSG:9377',
      }).readFeatures(geojson)
    );

    const vectorLayer = new VectorLayer({
      source: vectorSource,
      style: styleFunction,
    });
    if (this.map.getLayers().getLength() === 2) {
      this.map.getLayers().getArray().pop();
    }
    this.map.addLayer(vectorLayer);
    if (layer == 'search'){
      this.map.getView().fit(vectorSource.getExtent());
    }
  }


}
